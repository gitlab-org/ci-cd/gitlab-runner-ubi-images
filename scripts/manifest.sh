#!/bin/bash

PLATFORMS=${PLATFORMS:-linux/amd64}

manifest() {
    RUNNER_SOURCE="$BUILD_REPOSITORY/gitlab-runner-$PLATFORM_LOWERCASE:$RELEASE_SHA"
    HELPER_SOURCE="$BUILD_REPOSITORY/gitlab-runner-helper-$PLATFORM_LOWERCASE:$RELEASE_SHA"

    local RELEASE_SAFE=${RELEASE//\//-}
    RUNNER_MANIFESTS=(
      "$BUILD_REPOSITORY/gitlab-runner-$PLATFORM_LOWERCASE:$RELEASE_SAFE"
    )

    HELPER_MANIFESTS=(
      "$BUILD_REPOSITORY/gitlab-runner-helper-$PLATFORM_LOWERCASE:$RELEASE_SAFE"
    )

    if [[ "$RELEASE" != "$RELEASE_SHA" ]]; then
      RUNNER_MANIFESTS+=($RUNNER_SOURCE)
      HELPER_MANIFESTS+=($HELPER_SOURCE)
    fi

    echo "$CI_REGISTRY_PASSWORD" | docker login --username "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"

    echo $RUNNER_MANIFESTS
    for RUNNER_MANIFEST in "${RUNNER_MANIFESTS[@]}"; do
      echo $(./scripts/get_manifest.py "$PLATFORMS" "$RUNNER_MANIFEST" "$RUNNER_SOURCE") | xargs docker manifest create
      docker manifest push "$RUNNER_MANIFEST"
      echo "Pushed $RUNNER_MANIFEST"
    done

    for HELPER_MANIFEST in "${HELPER_MANIFESTS[@]}"; do
      echo $(./scripts/get_manifest.py "$PLATFORMS" "$HELPER_MANIFEST" "$HELPER_SOURCE" 1) | xargs docker manifest create
      docker manifest push "$HELPER_MANIFEST"
      echo "Pushed $HELPER_MANIFEST"
    done
}
