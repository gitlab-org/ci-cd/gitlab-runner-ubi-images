OSARCH=${OSARCHS:-linux/amd64}
OS=$(echo "$OSARCH" | cut -f1 -d/)
ARCH=$(echo "$OSARCH" | cut -f2 -d/)
HELPER_ARCH=$(if [ "${ARCH}" == "amd64" ]; then echo "x86_64"; else echo "${ARCH}"; fi)

RELEASE="${2:-main}"
RELEASE_SHA="${3:-$RELEASE}"
BUILD_REPOSITORY="${4:-gitlab}"
DOCKERFILE_PREPEND="${5:-""}"

PLATFORM_LOWERCASE=$(echo "$DOCKERFILE_PREPEND" | tr "[:upper:]" "[:lower:]")

RUNNER_IMAGE=$(printf "%s/%s:%s" "${BUILD_REPOSITORY}" "gitlab-runner-${PLATFORM_LOWERCASE}" "${ARCH}-${RELEASE_SHA}")
HELPER_IMAGE=$(printf "%s/%s:%s" "${BUILD_REPOSITORY}" "gitlab-runner-helper-${PLATFORM_LOWERCASE}" "${HELPER_ARCH}-${RELEASE_SHA}")
if [[ $PLATFORM_LOWERCASE == "" ]]; then
  RUNNER_IMAGE=$(printf "%s/%s:%s" "${BUILD_REPOSITORY}" "gitlab-runner" "${ARCH}-${RELEASE_SHA}")
  HELPER_IMAGE=$(printf "%s/%s:%s" "${BUILD_REPOSITORY}" "gitlab-runner-helper" "${HELPER_ARCH}-${RELEASE_SHA}")
fi

RUNNER_BUCKET_URL="https://gitlab-runner-downloads.s3.amazonaws.com"
RUNNER_URL="${RUNNER_BUCKET_URL}/${RELEASE}"

RELEASE_BASE_URL="${RUNNER_BUCKET_URL}/ubi-images"
RELEASE_URL="${RELEASE_BASE_URL}/${RELEASE}"

GPG_KEY_URL="${RUNNER_BUCKET_URL}/gitlab-runner-key.asc"

WORKSPACE="${BASE_DIR}/build"
CACHE_DIR="/tmp/gitlab-runner-ubi"
LOGS_DIR="${WORKSPACE}/logs"
LICENSES_DIR="${WORKSPACE}/licenses"

RUNNER_BINARY="${WORKSPACE}/gitlab-runner"
HELPER_BINARY="${WORKSPACE}/gitlab-runner-helper"
TINI_BINARY="${WORKSPACE}/tini"

RUNNER_CONTEXT="${WORKSPACE}/runner"
HELPER_CONTEXT="${WORKSPACE}/helper"

CHECKSUM_FILE="${WORKSPACE}/release.sha256"
CHECKSUM_SIGNATURE_FILE="${WORKSPACE}/release.sha256.asc"
VALUES_YAML_FILE="${WORKSPACE}/ubi-values.yaml"

UBI_IMAGE=registry.gitlab.com/gitlab-org/gitlab-runner/ubi-fips-base
# renovate: datasource=docker depName=redhat/ubi9-micro versioning=redhat allowedVersions=/9\.4-.+/
UBI_VERSION=9.4-15
# UBI_VERSION should match UBI_MICRO_VERSION in https://gitlab.com/gitlab-org/gitlab-runner/-/blob/main/.gitlab/ci/_common.gitlab-ci.yml

# When building locally by default use :latest, we can still specify UBI_VERSION manually
BASE_IMAGE=$(printf "%s:%s" "${UBI_IMAGE}" "${UBI_VERSION:-latest}")

printMessage() {
    local INDENT="${1}"
    shift 1
    local MSG="${*}"

    local PREFIX="\xe2\x86\x92"
    if [[ ${INDENT} -gt 0 ]]; then
        PREFIX="\xe2\x86\xb3"
        for i in $(seq 1 "${INDENT}"); do
            PREFIX=" ${PREFIX}"
        done
    fi

    echo -e "\033[32;1m${PREFIX} ${MSG}\033[0;0m"
}
