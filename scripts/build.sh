RUNNER_REVISION="${RUNNER_REVISION:-}"

_buildImage() {
  local IMAGE_NAME="${1}"
  local TARGET_IMAGE="${2}"
  local CONTEXT="${3}"
  local VERSION="${4}"
  local DOCKERFILE="Dockerfile"

  if [[ -n $DOCKERFILE_PREPEND ]]; then
    DOCKERFILE="${DOCKERFILE_PREPEND}.Dockerfile"
  fi

  printMessage 0 "Building ${TARGET_IMAGE} image for ${OSARCH}"

  {
    docker buildx build \
      --platform "${OSARCH}" \
      -t "${TARGET_IMAGE}" \
      --output type=image \
      --build-arg "BASE_IMAGE=${BASE_IMAGE}" \
      --build-arg "VERSION=${VERSION}" \
      "${DOCKER_OPTS[@]}" \
      -f "${CONTEXT}/${DOCKERFILE}" \
      "${CONTEXT}" |& tee "${LOGS_DIR}/${IMAGE_NAME}.log"
  } || {
    echo "${IMAGE_NAME}" >>"${LOGS_DIR}/failed.log"
  }
}

_generateValuesYAML() {
  printMessage 0 "Generating ${VALUES_YAML_FILE} file"

  local USER_ID
  USER_ID="$(docker run --rm --entrypoint sh "${RUNNER_IMAGE}" -c "id -u gitlab-runner")"

  local GROUP_ID
  GROUP_ID="$(docker run --rm --entrypoint sh "${RUNNER_IMAGE}" -c "id -g nobody")"

  cat >"${VALUES_YAML_FILE}" <<EOF
securityContext:
  runAsUser: ${USER_ID}
  fsGroup: ${GROUP_ID}
image: ${RUNNER_IMAGE}
runners:
  helpers:
    image: ${HELPER_IMAGE}

EOF
}

_testImages() {
  docker run --rm "${RUNNER_IMAGE}" --version
  docker run --rm --entrypoint /bin/sh "${RUNNER_IMAGE}" -c "cat /etc/os-release"
  docker run --rm "${HELPER_IMAGE}" gitlab-runner-helper --version
  docker run --rm "${HELPER_IMAGE}" cat /etc/os-release
}

buildImages() {
  if [[ ! -d "${WORKSPACE}" ]]; then
    echo "Workspace is not initialized. Call 'prepare.sh' first!"
    exit 1
  fi

  # Enter the workspace. From now on everything will be executed
  # in this directory
  cd "${WORKSPACE}" || return 1

  _buildImage gitlab-runner "${RUNNER_IMAGE}" "${RUNNER_CONTEXT}" "${RELEASE_SHA}" "${OSARCH}"
  _buildImage gitlab-runner-helper "${HELPER_IMAGE}" "${HELPER_CONTEXT}" "${RELEASE_SHA}" "${OSARCH}"
  _testImages

  # For OpenShift we don't need to generate values.yaml. The security context
  # is managed by the OpenShift admin and the Runner Operator uses the project-level
  # security context
  if [[ -z $DOCKERFILE_PREPEND ]]; then
    _generateValuesYAML
  fi
}
