#!/bin/bash

push() {
    echo "$CI_REGISTRY_PASSWORD" | docker login --username "$CI_REGISTRY_USER" --password-stdin "$CI_REGISTRY"

    printMessage 1 "Pushing image '$RUNNER_IMAGE'"
    docker push "$RUNNER_IMAGE"

    printMessage 1 "Pushing image '$HELPER_IMAGE'"
    docker push "$HELPER_IMAGE"
}
