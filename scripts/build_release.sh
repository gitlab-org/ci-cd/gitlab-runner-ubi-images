_importGPGKey() {
  local KEY_FILE="${1}"
  local KEY_ID="${2}"

  printMessage 1 "Importing GPG key '${KEY_ID}'"

  gpg --import < "${KEY_FILE}"
  gpg --list-keys "${KEY_ID}"
}

_downloadGitLabRunnerWithHelper() {
  _importGPGKey "${GITLAB_RUNNER_PUBLIC_GPG_KEY}" "${GITLAB_RUNNER_PUBLIC_GPG_KEY_ID}"

  printMessage 1 "Downloading GitLab Runner '${RELEASE}' with the helper binary for ${ARCH}"

  local WORKDIR="./gitlab-runner"
  local RUNNER_BINARY="${WORKDIR}/binaries/gitlab-runner-linux-${ARCH}"
  local HELPER_BINARY="${WORKDIR}/binaries/gitlab-runner-helper/gitlab-runner-helper.${HELPER_ARCH}"
  local CHECKSUM_FILE="${WORKDIR}/release.sha256"
  local CHECKSUM_SIGNATURE_FILE="${WORKDIR}/release.sha256.asc"

  mkdir -p "${WORKDIR}/binaries/gitlab-runner-helper"

  curl -Lf "${RUNNER_URL}/binaries/gitlab-runner-linux-${ARCH}" -o "${RUNNER_BINARY}"
  curl -Lf "${RUNNER_URL}/binaries/gitlab-runner-helper/gitlab-runner-helper.${HELPER_ARCH}" -o "${HELPER_BINARY}"
  curl -Lf "${RUNNER_URL}/release.sha256" -o "${CHECKSUM_FILE}"
  curl -Lf "${RUNNER_URL}/release.sha256.asc" -o "${CHECKSUM_SIGNATURE_FILE}"

  cd "${WORKDIR}" || return 1

  printMessage 1 "Verifying GitLab Runner binaries integrity"

  gpg --verify release.sha256.asc

  sha256sum --check --strict --ignore-missing release.sha256

  cd .. || return 1

  if [[ "${ARCH}" == "amd64" ]]; then
    cp "${RUNNER_BINARY}" "dist/gitlab-runner"
    cp "${HELPER_BINARY}" "dist/gitlab-runner-helper"
  fi

  mv "${RUNNER_BINARY}" "dist/gitlab-runner-${ARCH}"
  mv "${HELPER_BINARY}" "dist/gitlab-runner-helper-${ARCH}"
}

buildRelease() {
  printMessage 0 "Building release"

  mkdir -p build/index/dist
  cd build/index || return 1

  _downloadGitLabRunnerWithHelper
}
