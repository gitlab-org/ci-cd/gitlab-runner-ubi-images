TINI_VERSION=0.19.0
SKIP_INTEGRITY_VERIFICATION=false

_initializeWorkspace() {
  printMessage 0 "Initializing workspace"

  mkdir -p "${LOGS_DIR}"
  mkdir -p "${CACHE_DIR}-${ARCH}"

  rm -f "${LOGS_DIR}"/*
  rm -rf "${RUNNER_CONTEXT}"
  rm -rf "${HELPER_CONTEXT}"

  cp -a runner/ "${RUNNER_CONTEXT}"
  cp -a helper/ "${HELPER_CONTEXT}"
}

_downloadAndImportGPGKey() {
  printMessage 0 "Initializing GPG Key"

  printMessage 1 "Downloading ${GPG_KEY_URL}"
  curl -Lf "${GPG_KEY_URL}" -o key
  gpg --import key
  gpg --list-keys "$(gpg --list-packets <key | awk '$1=="keyid:"{print$2}' | head -n 1)"
}

_downloadBinary() {
  local URL="${1}"
  local CACHE="${CACHE_DIR}-${ARCH}/${2}-${RELEASE}"
  local TARGET="${3}"

  printMessage 1 "Downloading ${URL}"

  if [[ ! -f "${CACHE}" ]]; then
    curl -Lf "${URL}" -o "${CACHE}"
  else
    printMessage 2 "Using cached binary from ${CACHE}"
  fi

  cp "${CACHE}" "${TARGET}"
  chmod +x "${TARGET}"
}

_downloadTini() {
  printMessage 0 "Downloading tini"
  
  _importGPGKey "${TINI_PUBLIC_GPG_KEY}" "${TINI_PUBLIC_GPG_KEY_ID}"

  curl -Lf "https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini-static-${ARCH}" -o "./tini-${ARCH}"
  curl -Lf "https://github.com/krallin/tini/releases/download/v${TINI_VERSION}/tini-static-${ARCH}.asc" -o "./tini-${ARCH}.asc"

  printMessage 2 "Verifying Tini integrity"

  gpg --verify "./tini-${ARCH}.asc"

  mv "./tini-${ARCH}" "${TINI_BINARY}-${ARCH}"
}

_downloadBinaries() {
  printMessage 0 "Downloading Runner and Helper binaries"

  RUNNER_BINARY_ARTIFACT_LOCATION="out/binaries/gitlab-runner-${OS}-${ARCH}"
  HELPER_BINARY_ARTIFACT_LOCATION="out/binaries/gitlab-runner-helper/gitlab-runner-helper.${OS}-${ARCH}"
  if [[ -f "${RUNNER_BINARY_ARTIFACT_LOCATION}" && -f "${HELPER_BINARY_ARTIFACT_LOCATION}" ]]; then
    printMessage 1 "Using cached binaries from ${RUNNER_BINARY_ARTIFACT_LOCATION} and ${HELPER_BINARY_ARTIFACT_LOCATION}"
    mv "${RUNNER_BINARY_ARTIFACT_LOCATION}" "${RUNNER_BINARY}-${ARCH}"
    mv "${HELPER_BINARY_ARTIFACT_LOCATION}" "${HELPER_BINARY}-${ARCH}"
    SKIP_INTEGRITY_VERIFICATION=true
    return
  fi

  # Cache binaries but always download the checksum file and its signature
  _downloadBinary "${RELEASE_URL}/gitlab-runner-${ARCH}" "gitlab-runner-${ARCH}" "${RUNNER_BINARY}-${ARCH}"
  _downloadBinary "${RELEASE_URL}/gitlab-runner-helper-${ARCH}" "gitlab-runner-helper-${ARCH}" "${HELPER_BINARY}-${ARCH}"

  printMessage 1 "Downloading ${RELEASE_URL}/release.sha256"
  curl -Lf "${RELEASE_URL}/release.sha256" -o "${CHECKSUM_FILE}"

  printMessage 1 "Downloading ${RELEASE_URL}/release.sha256.asc"
  curl -Lf "${RELEASE_URL}/release.sha256.asc" -o "${CHECKSUM_SIGNATURE_FILE}"
}

_verifyDependenciesIntegrity() {
  if [ $SKIP_INTEGRITY_VERIFICATION = true ]; then
    echo "Skipping integrity verification. Using parent pipeline binaries."
    return
  fi
  
  printMessage 0 "Verifying dependencies integrity"

  gpg --verify "${CHECKSUM_SIGNATURE_FILE}"

  sha256sum --check --strict --ignore-missing "${CHECKSUM_FILE}"
}

_prepareLicenses() {
  mkdir -p "${LICENSES_DIR}"
  cp LICENSE "${LICENSES_DIR}/"
}

_finalizeWorkspacePreparation() {
  printMessage 0 "Finalizing workspace initialization"

  mv "${RUNNER_BINARY}-${ARCH}" "${RUNNER_BINARY}"
  mv "${HELPER_BINARY}-${ARCH}" "${HELPER_BINARY}"
  mv "${TINI_BINARY}-${ARCH}" "${TINI_BINARY}"

  cp "${RUNNER_BINARY}" "${RUNNER_CONTEXT}"
  cp "${TINI_BINARY}" "${RUNNER_CONTEXT}"
  cp -r "${LICENSES_DIR}" "${RUNNER_CONTEXT}"

  cp "${HELPER_BINARY}" "${HELPER_CONTEXT}"
  cp -r "${LICENSES_DIR}" "${HELPER_CONTEXT}"
}

prepareImageBuild() {
  _initializeWorkspace
  _downloadAndImportGPGKey
  _prepareLicenses
  _downloadTini
  _downloadBinaries

  # Enter the workspace. From now on everything will be executed
  # in this directory
  cd "${WORKSPACE}" || return 1

  _verifyDependenciesIntegrity
  _finalizeWorkspacePreparation
}
