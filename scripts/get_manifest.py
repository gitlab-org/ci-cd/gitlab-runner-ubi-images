#!/usr/bin/env python3

import os
import sys


def map_arch_image(platform, image, version, is_helper):
    arch = platform.split("/")[1]
    if is_helper and arch == "amd64":
        arch = "x86_64"

    return "{}:{}-{}".format(image, arch, version)


def split_manifest_and_tag(manifest):
    split = manifest.split(":")
    return split[0], split[1]


def main():
    platforms = sys.argv[1].split(',')
    manifest = sys.argv[2]
    source = sys.argv[3]
    is_helper = len(sys.argv) > 4

    image, version = split_manifest_and_tag(source)
    arch_images = map(lambda platform: map_arch_image(
        platform, image, version, is_helper), platforms)

    print(manifest, ' '.join([str(i) for i in arch_images]))


main()
