ARG BASE_IMAGE

FROM ${BASE_IMAGE}

ARG VERSION
LABEL name="GitLab Runner Helper" \
    vendor="GitLab" \
    maintainer="support@gitlab.com" \
    version="${VERSION}" \
    release="${VERSION}" \
    summary="GitLab Runner used to conduct CI/CD jobs on Kubernetes. The helper contains a subset of specialized commands used when running jobs." \
    description="Contains a gitlab-runner-helper binary which is a special compilation of GitLab Runner binary, that contains only a subset of available commands, as well as Git, Git LFS, SSL certificates store. This image will run alongside each build container, in the same pod."

ENV HOME=/home/gitlab-runner

COPY --chown=0:0 --chmod=555 ./gitlab-runner-build.sh /usr/local/bin/gitlab-runner-build
COPY --chown=0:0 --chmod=555 ./gitlab-runner-helper /usr/bin/gitlab-runner-helper

COPY ./licenses /licenses

# mock `adduser -r -m -d $HOME gitlab-runner`, for ubi-micro
RUN echo "gitlab-runner:x:1001:1001::${HOME}:/bin/bash" >> /etc/passwd && \
    echo 'gitlab-runner:x:1001:' >> /etc/group && \
    echo 'gitlab-runner:!!:19930::::::' >> /etc/shadow && \
    echo 'gitlab-runner:!::' >> /etc/gshadow && \
    install -d -m 770 -o 1001 -g 0 ${HOME} \
    # check ID
    id gitlab-runner

# https://docs.openshift.com/container-platform/4.6/openshift_images/create-images.html#support-arbitrary-user-ids
RUN install -d -m 770 -o 1001 -g 0 /builds /cache

USER 1001

CMD ["sh"]
