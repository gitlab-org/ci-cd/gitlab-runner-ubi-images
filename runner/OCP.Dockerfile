ARG BASE_IMAGE

FROM ${BASE_IMAGE}

ARG VERSION
LABEL name="GitLab Runner" \
    vendor="GitLab" \
    maintainer="support@gitlab.com" \
    version="${VERSION}" \
    release="${VERSION}" \
    summary="GitLab Runner used to conduct CI/CD jobs on Kubernetes" \
    description="GitLab Runner is used to run CI/CD jobs from GitLab on Kubernetes/OpenShift Platforms. This image will run each CI/CD Build in a separate pod."

ENV HOME=/home/gitlab-runner

COPY --chown=0:0 --chmod=555 ./tini /usr/local/bin/tini
COPY --chown=0:0 --chmod=555 ./gitlab-runner /usr/local/bin/gitlab-runner
COPY --chown=0:0 --chmod=555 ./entrypoint.sh /entrypoint

COPY ./licenses /licenses

# https://docs.openshift.com/container-platform/4.6/openshift_images/create-images.html#support-arbitrary-user-ids
RUN install -d -m 770 -o 1001 -g 0 /etc/gitlab-runner/certs ${HOME} /secrets

USER 1001

STOPSIGNAL SIGQUIT
ENTRYPOINT ["tini", "--", "/entrypoint"]
CMD ["run", "--working-directory=/"]
